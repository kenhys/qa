#!/usr/bin/perl
# +-------------------------------------------------------------------------+
# | Copyright (C) 2012 Christoph Berg <myon@debian.org>                     |
# |                                                                         |
# | This program is free software; you can redistribute it and/or           |
# | modify it under the terms of the GNU General Public License             |
# | as published by the Free Software Foundation; either version 2          |
# | of the License, or (at your option) any later version.                  |
# |                                                                         |
# | This program is distributed in the hope that it will be useful,         |
# | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
# | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           |
# | GNU General Public License for more details.                            |
# +-------------------------------------------------------------------------+

use warnings;
use strict;
use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use DBD::Pg;
use HTML::Entities;
use JSON;
use Template;
use Time::HiRes qw(time);
use URI::Escape;

my $start_time = time;

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

my $cgi = new CGI;
my $dbh = DBI->connect("dbi:Pg:service=udd", '', '',
	{ AutoCommit => 1, RaiseError => 1, PrintError => 1});
$dbh->do ("SET standard_conforming_strings = on; SET search_path TO ddpo, apt, public");

#####################################################################################

my $pkg = $cgi->param('pkg') || '';

my $status = "200 Ok";
my $title = "Watch";
my $body = <<EOF;
<h1>Watch file results</h1>
<p>
<form name="f1" action="watch" method="GET">
<input type="text" name="pkg" value="$pkg" size="30" maxlength="80" />
<input type="submit">
</form>
</p>
EOF

if ($pkg) {
	my $q = $dbh->prepare ("SELECT * FROM upstream WHERE source = ?");
	$q->execute ($pkg);
	my $row = $q->fetchrow_hashref;
	if ($row) {
		if ($cgi->param('json')) {
			print header (-type => 'application/json', -charset => 'utf-8');
			print encode_json($row);
			exit;
		}

		foreach my $key (sort keys %$row) {
			my $d = $row->{$key} or next;
			$d = "<a href=\"$d\">$d</a>" if ($key eq "upstream_url");
			$d = "<pre>$d</pre>" if ($key eq "watch_file");
			$body .= "$key: <b>$d</b><br />\n";
		}
	} else {
		$status = "404 Package not found";
		$body .= "Package not found";
	}

} else {
	$status = "404 Missing parameter";
}

#####################################################################################

print header (-charset => 'utf-8', -status => $status);

my $template = Template->new (INCLUDE_PATH => '/srv/qa.debian.org/web');

$template->process('qa-template.html', {
	TITLE => $title,
	BODY => $body,
	AUTHOR => 'Christoph Berg',
	GENERATED => sprintf "Rendering took %.3fs.\n", time - $start_time,
}) || die $template->error();

$dbh->disconnect;
