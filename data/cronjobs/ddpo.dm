#!/bin/bash -e
# CRON=43 * * * *

# read https://ftp-master.debian.org/dm.txt for use in DDPO
umask 002
cd /srv/qa.debian.org/data/ddpo/results
rm -f dm-new.db
../extract_dm.pl
mv -f dm-new.db dm.db
echo `date -u` > dm.date

