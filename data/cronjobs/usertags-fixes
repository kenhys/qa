#!/bin/bash
# Run BTS commands to fix typos and other usertags issues
# CRON=46 19 * * *

set -e

delete_tags () {
	local dir="$1"
	local from="$2"
	local -n __dt_from_tags=$3
	local  __dt_to_tags=()
	transfer_one "$dir" "$from" __dt_from_tags '' __dt_to_tags
}

rename_tags () {
	local dir="$1"
	local -n __rt_from_tags=$2
	local -n __rt_to_tags=$3
	transfer_all "$dir" __rt_from_tags __rt_to_tags
}

rename_user_tags () {
	local dir="$1"
	local from="$2"
	local -n __rtt_from_tags=$3
	local to="$from"
	local -n __rtt_to_tags=$4
	transfer_one "$dir" "$from" __rtt_from_tags "$to" __rtt_to_tags
}

rename_transfer_tags () {
	local dir="$1"
	local from="$2"
	local -n __rtt_from_tags=$3
	local to="$4"
	local -n __rtt_to_tags=$5
	transfer_one "$dir" "$from" __rtt_from_tags "$to" __rtt_to_tags
}

transfer_tags () {
	local dir="$1"
	local from="$2"
	local -n __tt_from_tags=$3
	local to="$4"
	local __tt_to_tags=("${__tt_from_tags[@]}")
	transfer_one "$dir" "$from" __tt_from_tags "$to" __tt_to_tags
}

copy_tags () {
	local dir="$1"
	local -n __ct_from_tags=$2
	local to="$3"
	local __ct_to_tags=("${__t_tags[@]}")
	transfer_all_to "$dir" __ct_from_tags "$to" __ct_to_tags
}

transfer_all_tags () {
	local from="$1"
	local to="$2"
	local from_tags=()
	< <(sed --quiet "s/Tag: //p" "$from") \
	readarray -t from_tags
	local to_tags=("${from_tags[@]}")
	transfer "$from" from_tags "$to" to_tags
}

find_and_correct () {
	local f
	test -d "$1" || return 0
	find "$1/" -type f | while read -r f ; do
		transfer_all_tags "$f" "$(basename "$f" | sed "$2")"
	done
}

copy () {
	dir="fixes/$2/"
	mkdir --parents "$dir"
	find all -type f -wholename "all/*/$1" ! -wholename "all/*.tmp/$1" -print0 |
	xargs --null --no-run-if-empty cp --target-directory "$dir"
}

transfer_one () {
	local dir="$1"
	local from="$2"
	local -n __to_from_tags=$3
	local to="$4"
	local -n __to_to_tags=$5
	local f
	find "$dir" -type f -name "$from" | while read -r f ; do
		transfer "$f" __to_from_tags "$to" __to_to_tags
	done
}

transfer_all () {
	local dir="$1"
	local -n __ta_from_tags=$2
	local -n __ta_to_tags=$3
	local f
	find "$dir" -type f | while read -r f ; do
		transfer "$f" __ta_from_tags "$f" __ta_to_tags
	done
}

transfer_all_to () {
	local dir="$1"
	local -n __tat_from_tags=$2
	local to="$3"
	local -n __tat_to_tags=$4
	local f
	find "$dir" -type f | while read -r f ; do
		transfer "$f" __tat_from_tags "$to" __tat_to_tags
	done
}

transfer () {
	local CC="$CC"
	local COMMENT="$COMMENT"
	local __t_from="$1"
	local __t_from_mail
	__t_from_mail="$(basename "$1")"
	local -n __t_from_tags=$2
	local __t_to="$3"
	local __t_to_mail
	__t_to_mail="$(basename "$3")"
	local -n __t_to_tags=$4
	local __t_copy="$5"
	local __t_bts=()
	for __t_i in "${!__t_from_tags[@]}"; do
		local __t_bugs=()
		__t_from_tag="${__t_from_tags[$__t_i]}"
		< <(
			sed --quiet "/^Tag: ${__t_from_tag}$/,/^$/{/^Tag: /d;p}" "$__t_from" |
			grep --only-matching '[0-9]*'
		) readarray -t __t_bugs
		if [ "${#__t_bugs[@]}" -eq 0 ] ; then
			continue
		fi
		if [ "$__t_copy" != 1 ] ; then
			__t_bts+=("user" "$__t_from_mail")
			if [ -n "$COMMENT" ] ; then
				__t_bts+=("# $COMMENT")
			fi
			__t_bts+=(",")
			for __t_bug in "${__t_bugs[@]}" ; do
				__t_bts+=("usertags" "$__t_bug" "-" "$__t_from_tag" ",")
			done
		fi
		if [ -n "$__t_to_mail" ] && [ "${#__t_to_tags[@]}" -gt "$__t_i" ] ; then
			__t_bts+=("user" "$__t_to_mail" ",")
			__t_to_tag="${__t_to_tags[$__t_i]}"
			for __t_bug in "${__t_bugs[@]}" ; do
				__t_bts+=("usertags" "$__t_bug" "+" "$__t_to_tag" ",")
			done
		fi
	done
	if [ "${#__t_bts[@]}" -ne 0 ] ; then
		if [ -n "$CC" ] ; then
			__t_bts=("--cc-addr=$CC" "${__t_bts[@]}")
		fi
		if [ "$debug" -ne 1 ] ; then
			bts "${__t_bts[@]}"
		else
			printf '%q ' bts "${__t_bts[@]}" | sed 's/ $/\n/'
		fi
	fi
}

if [ "$1" = --debug ] ; then
	debug=1
	shift
else
	debug=0
fi

if [ $# -eq 1 ] ; then
	scratchdir="$1"
elif [ $# -eq 0 ] ; then
	scratchdir=/srv/scratch/qa.debian.org/usertags
else
	echo "Please pass a directory for the script to operate in" 1>&2
	exit 1
fi
mkdir --parents "$scratchdir"
cd "$scratchdir"
if [ "$debug" -ne 1 ] ; then
	rsync --timeout=60 --recursive --delete rsync://bugs-mirror.debian.org/bts-spool-index/user/ all/
fi
# tagsregex=$(ssh bugs-master.debian.org "sed '/^1;$/i print join \"|\", @gTags;' < /srv/bugs.debian.org/etc/config | perl")
#tagsregex="patch|wontfix|moreinfo|unreproducible|fixed|potato|woody|sid|help|security|upstream|pending|sarge|sarge-ignore|experimental|d-i|confirmed|ipv6|lfs|fixed-in-experimental|fixed-upstream|l10n|newcomer|etch|etch-ignore|lenny|lenny-ignore|squeeze|squeeze-ignore|wheezy|wheezy-ignore|jessie|jessie-ignore|stretch|stretch-ignore|buster|buster-ignore|bullseye|bullseye-ignore"
#grep -rE "^Tag: ($tagsregex)$" all || true
rm --recursive --force fixes
mkdir fixes
copy '*.og' og
copy '*.or' or
copy '*.ort' ort
copy '*.ogr' ogr
copy '*.orf' orf
copy '*.rog' rog
copy '*.orge' orge
copy '*.orgu' orgu
copy '*.orrg' orrg
copy '*.d.o' d.o
copy '*.d.n' d.n
copy '*debia[.-]*' debia
copy '*debiarn*' debiarn
copy '*deabian*' deabian
copy '*dbeian*' dbeian
copy '*debain*' debain
copy '*deban*' deban
copy '*debin*' debin
copy '*.debian' debian
copy '*.debian-org' debian-org
copy '*%40debian-org' at-debian-org
copy '*.debian-net' debian-net
copy '*%40debian-net' at-debian-net
copy '*.debian-com' debian-com
copy '*%40debian-com' at-debian-com
#copy '*.debian.net' debian.net
copy '*%40debian.net' at-debian.net
copy '*.debian.com' debian.com
copy '*%40debian.com' at-debian.com
copy '*packages.org' packages.org
copy '*packages.qa.debian.org' packages.qa
copy '*packge*.debian.org' packge
copy '*pacakge*.debian.org' pacakge
copy '*package.debian.org' package
copy '*package*-debian-org' package-debian-org
copy '*list.debian.org' list
copy '*litst.debian.org' litst
copy '*%40alioth.debian.org' alioth
copy '*.debian.org*bugs.debian.org' qa
copy '*.debian.org*lists.debian.org' qa
copy 'debian-m68k%40lists.debian.org' 68k
copy 'debian-s390x%40lists.debian.org' s390
copy 'debian-power%40lists.debian.org' power
copy 'debian-ppc%40lists.debian.org' ppc
copy '*%40wdebian.org' wdebian.org
copy '*mulitarch*' mulitarch
copy 'reportbugs%40packages.debian.org' reportbugs
copy 'wnpp%40debian.org' dopdo
copy 'mirrors%40debian.org' dopdo
copy 'ubuntu-devel%40lists.debian.org' ubuntu
copy 'debian-ci%40*.debian.org' ddodldo
copy 'debian-emacsen%40*.debian.org' ddodldo
copy 'debian-gis%40*.debian.org' ddodldo
copy 'debian-live%40*.debian.org' ddodldo
copy 'debian-security%40*.debian.org' ddodldo
copy 'debian-policy%40*.debian.org' ddodpdo
copy 'debian-ocaml%40lists.debian.org' ocaml
copy 'debian-cross%40lists.debian.org' cross
copy 'debian-ci%40lists.debian.org' ci
copy 'nilesh%40debian.org' nilesh
copy 'ginggs%40debian.org' ginggs
copy 'elbrus%40debian.org' elbrus
(
cd fixes
find . -type f -print0 | xargs --null --no-run-if-empty rename 's/%40/@/'
find . -mindepth 2 -empty -delete
find_and_correct og 's/\.og$/.org/'
find_and_correct or 's/\.or$/.org/'
find_and_correct ort 's/\.ort$/.org/'
find_and_correct ogr 's/\.ogr$/.org/'
find_and_correct orf 's/\.orf$/.org/'
find_and_correct rog 's/\.rog$/.org/'
find_and_correct orge 's/\.orge$/.org/'
find_and_correct orgu 's/\.orgu$/.org/'
find_and_correct orrg 's/\.orrg$/.org/'
find_and_correct d.o 's/\.d\.o$/.debian.org/'
find_and_correct d.n 's/\.d\.n$/.debian.net/'
find_and_correct debiarn 's/debiarn/debian/g'
find_and_correct deabian 's/deabian/debian/g'
find_and_correct dbeian 's/dbeian/debian/g'
find_and_correct debain 's/debain/debian/g'
find_and_correct debia 's/debia\([-.]\)/debian\1/g'
find_and_correct deban 's/deban/debian/g'
find_and_correct debin 's/debin/debian/g'
find_and_correct debian 's/\.debian$/&.org/'
find_and_correct debian-org 's/debian-org/debian.org/'
find_and_correct at-debian-org 's/@debian-org/@debian.org/'
find_and_correct package-debian-org 's/packages\?-debian-org/packages.debian.org/'
find_and_correct debian-net 's/debian-net/debian.net/'
find_and_correct at-debian-net 's/@debian-net/@debian.net/'
find_and_correct debian-com 's/debian-com/debian.com/'
find_and_correct at-debian-com 's/@debian-com/@debian.com/'
find_and_correct debian.com 's/\.debian\.com$/.debian.org/'
find_and_correct at-debian.com 's/@debian\.com$/@debian.org/'
#find_and_correct debian.net 's/\.debian\.net$/.debian.org/'
find_and_correct at-debian.net 's/@debian\.net$/@debian.org/'
find_and_correct packages.org 's/packages\.org$/packages.debian.org/'
find_and_correct packages.qa 's/packages\.qa\.debian\.org$/packages.debian.org/'
find_and_correct package 's/package\.debian\.org$/packages.debian.org/'
find_and_correct packge 's/packges\?\.debian\.org$/packages.debian.org/'
find_and_correct pacakge 's/pacakges\?\.debian\.org$/packages.debian.org/'
find_and_correct list 's/list\.debian\.org$/lists.debian.org/'
find_and_correct litst 's/litst\.debian\.org$/lists.debian.org/'
find_and_correct qa 's/\.debian\.org@\(bugs\|lists\)\.debian\.org$/.debian.org@packages.debian.org/'
find_and_correct alioth 's/@alioth\.debian\.org$/@lists.alioth.debian.org/'
find_and_correct 68k 's/m68k/68k/g'
find_and_correct s390 's/s390x/s390/g'
find_and_correct power 's/power@/powerpc@/'
find_and_correct ppc 's/ppc/powerpc/g'
find_and_correct mulitarch 's/mulitarch/multiarch/g'
find_and_correct reportbugs 's/reportbugs/reportbug/g'
find_and_correct wdebian.org 's/@wdebian.org/@debian.org/'
find_and_correct dopdo 's/@debian\.org$/@packages.debian.org/'
find ddodldo -name 'debian-*@lists.debian.org' -delete
find ddodpdo -name 'debian-*@packages.debian.org' -delete
find_and_correct ddodldo 's/^\(debian-[^@]\+\)@.*\.debian\.org$/\1@lists.debian.org/'
find_and_correct ddodpdo 's/^\(debian-[^@]\+\)@.*\.debian\.org$/\1@packages.debian.org/'
find_and_correct ocaml 's/ocaml/ocaml-maint/'

cross_rename_tags_from=(ftcbffs ftbcfs cross cross-build)
cross_rename_tags_to=(ftcbfs ftcbfs ftcbfs ftcbfs)
rename_tags cross cross_rename_tags_from cross_rename_tags_to

nilesh_transfer=(ftcbfs)
CC="Nilesh Patra <nilesh@debian.org>" transfer_tags nilesh nilesh@debian.org nilesh_transfer debian-cross@lists.debian.org

ci_transfer_tags=(fails-always regression issue needs-update breaks needs-internet flaky superficial timeout)
CC="Graham Inggs <ginggs@debian.org>" transfer_tags ginggs ginggs@debian.org ci_transfer_tags debian-ci@lists.debian.org
CC="Paul Gevers <elbrus@debian.org>" transfer_tags elbrus elbrus@debian.org ci_transfer_tags debian-ci@lists.debian.org

ci_rename_tags_from=(timesout always-fails)
ci_rename_tags_to=(timeout fails-always)
CC="Paul Gevers <elbrus@debian.org>" rename_tags ci ci_rename_tags_from ci_rename_tags_to
)
if [ "$debug" -ne 1 ] ; then
	rm -rf fixes
fi
