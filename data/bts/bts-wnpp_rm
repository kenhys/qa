#!/usr/bin/perl

# Summary wnpp and removal request entries for use in QA pages
# Copyright (C) 2004, 2005  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
# Copyright (C) 2006-2015  Christoph Berg <myon@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# wnpp_rm output definition:
# - key-value pairs of source package name and wnpp/rm status
# - the plain text file is <key>:\s+<value>
# - the value is one or more '|'-separated times "<type>\s+<bugnr>[\s+
#   reserved for future expansions]"
# - type is one of RFP, ITP, O, RFA, RFH, ITA, RM, possibly more later

# wnpp_by_maint output definition:
# - key-value pairs address - list of bugs
# - key is s:<address> (submitter), or o:<address> (owner)
# - value is |-concatenated list of <bugnr> <unix time> <bug title>
# - similarly, the sp:address and op:address entries are space-separated
#   package lists
# - tag:<tag> summarizes all ITP/O/... bugs

use strict;
use DB_File;
use lib '/usr/share/devscripts';
use Devscripts::Debbugs;

my $OUT = "wnpp_rm";
my $OUT_orphaned = "wnpp_orphaned";
my $MAINT_OUT = "wnpp_by_maint";
my (%results, %maint_results);

my $wnppbugs = Devscripts::Debbugs::select ("package:wnpp",
		"status:forwarded", "status:open");
my $ftpbugs = Devscripts::Debbugs::select ("package:ftp.debian.org",
		"status:forwarded", "status:open");
my $sponsorbugs = Devscripts::Debbugs::select ("package:sponsorship-requests",
		"status:forwarded", "status:open");
my @bugs = (@$wnppbugs, @$ftpbugs, @$sponsorbugs);

sub devscripts_debbugs_status # TODO revert this once a newer devscripts does this
{
	my $bugs = shift;
	my @bugs = @$bugs;
	my %status;

	while( my @part = splice( @bugs, 0, 1000 ) )
	{
		my $partstatus = Devscripts::Debbugs::status (\@part);
		%status = ( %status, %$partstatus );
	}

	return \%status;
}

#my $status = Devscripts::Debbugs::status (\@bugs);
my $status = devscripts_debbugs_status( \@bugs );

for my $bugnr (@bugs) {
	my $title = $status->{$bugnr}->{subject};
	my $submitter = lc $status->{$bugnr}->{originator};
	my $owner = lc $status->{$bugnr}->{owner};
	my $time = lc $status->{$bugnr}->{date};
	$submitter =~ s/.*<(.*)>.*/$1/;
	$owner =~ s/.*<(.*)>.*/$1/;

	# TAG: pkg, pkg [arch1, arch2] --? title
	# RFS: netplug/1.2.9.2-2 [ITA]
	$title .= " -- (no description)" unless ($title =~ / -/);
	next unless ($title =~ /^(RFP|ITP|ITO|O|RFA|RFH|ITA|RM|RFS):\s+(.*)/i);
	my ($type, $packages) = (uc($1), lc($2));

	$title =~ s/\|/&124;/g; # protect encoding
	push @{$maint_results{"s:$submitter"}}, "$bugnr $time $title";
	push @{$maint_results{"o:$owner"}}, "$bugnr $time $title" if $owner;
	while ($packages =~ s/^([\w][\w.+-]+),? ?//) { # eat prefix as long as it looks like a package name
		my $package = $1;
		push @{$results{$package}}, "$type $bugnr";
		push @{$maint_results{"sp:$submitter"}}, $package;
		push @{$maint_results{"op:$owner"}}, $package if $owner;
		push @{$maint_results{"tag:$type"}}, $package;
	}
}

my (%db, %db_orphaned, %maint_db);
my $db_file = tie %db, "DB_File", "$OUT.new.db",
	O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE or die "Can't open database $OUT.new.db : $!";
my $db_file_orphaned = tie %db_orphaned, "DB_File", "$OUT_orphaned.new.db",
	O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE or die "Can't open database $OUT_orphaned.new.db : $!";

open OUT, ">$OUT.new" or die "Error opening plaintext output file";
foreach my $k (sort keys %results) {
	print OUT "$k: ". join ('|', @{$results{$k}}) . "\n";
	$db{$k} = join ('|', @{$results{$k}});
	foreach my $result ( @{$results{$k}} ) {
		$db_orphaned{$k} = $1 if( $result =~ /^O (\d+)$/ );
	}
}
close OUT;
close DB;

my $maint_db_file = tie %maint_db, "DB_File", "$MAINT_OUT.new.db",
	O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE or die "Can't open database $MAINT_OUT.new.db : $!";

open OUT, ">$MAINT_OUT.new" or die "Error opening plaintext output file";
foreach my $k (sort keys %maint_results) {
	my $sep = $k =~ /^([so]p|tag):/ ? ' ' : '|';
	my $data = join ($sep, sort @{$maint_results{$k}});
	print OUT "$k: $data\n";
	$maint_db{$k} = $data;
}
close OUT;
close DB;

rename "$OUT.new", "$OUT";
rename "$OUT.new.db", "$OUT.db";
rename "$OUT_orphaned.new", "$OUT_orphaned";
rename "$OUT_orphaned.new.db", "$OUT_orphaned.db";
rename "$MAINT_OUT.new", "$MAINT_OUT";
rename "$MAINT_OUT.new.db", "$MAINT_OUT.db";

